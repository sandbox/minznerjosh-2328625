<?php

/**
 * @file
 * Functions for parsing/replacing MiniReel shortcodes.
 *
 * The functions contained in this file are used to convert MiniReel shortcodes
 * found in node contents into Cinema6 embed script tags.
 */

/**
 * Creates a single function from a pipeline of functions.
 *
 * Returns the composition of a list of functions, where each function consumes
 * the return value of the function that precedes. In math terms, composing the
 * functions f(), g(), and h() produces f(g(h())).
 *
 * @param function[] $fns
 *   An array of functions to pipeline.
 * @param $context
 *   An optional context value that will be passed as the second parameter to
 *   every parameter in the composition.
 *
 * @return function $fn
 *   A function that is the composition of all functions supplied.
 */
function _cinema6_minireel_pipeline(array $fns, &$context = NULL) {
  return function($initial) use ($fns, &$context) {
    return array_reduce($fns, function($value, $next_fn) use (&$context) {
      return $next_fn($value, $context);
    }, $initial);
  };
}

/**
 * Converts MiniReel shortcodes into script tags.
 *
 * Finds instances of MiniReel shortcodes in the HTML e.g.:
 *
 * [minireel version="1" exp="e-abc" splash="img-only:6/5"]
 *
 * and converts them into MiniReel embed script tags e.g.:
 *
 * <script src="//lib.Xcinema6.com/c6embed/v1/c6embed.min.js" data-exp="e-abc"
 *   data-splash="img-only:6/5"></script>
 *
 * @param string $html
 *   The HTML of the page that contains shortcodes.
 *
 * @return string $html.
 *   The HTML with converted shortcodes.
 */
function _cinema6_minireel_replace($html) {
  /*
   * Converts this:
   * '[minireel version="1" exp="e-abc" splash="img-only:6/5"]'
   * Into this:
   * [
   *     [
   *       'attr'  => 'version',
   *       'value' => '1'
   *     ],
   *     [
   *       'attr'  => 'exp',
   *       'value' => 'e-abc'
   *     ],
   *     [
   *       'attr'  => 'splash',
   *       'value' => 'img-only:6/5'
   *     ]
   * ]
   */
  $arrayify_shortcode = _cinema6_minireel_pipeline([
    function($code) {
      $matches = [];

      preg_match_all('/\w+=".+?"/', $code, $matches);

      return $matches[0];
    },
    function($pairs) {
      return array_map(function($pair) {
        $matches = [];

        preg_match('/(\w+)="(.+?)"/', $pair, $matches);

        return [
          'attr' => $matches[1],
          'value' => $matches[2]
        ];
      }, $pairs);
    }
  ]);

  /*
   * Converts this:
   * [
   *     [
   *       'attr'  => 'version',
   *       'value' => '1'
   *     ],
   *     [
   *       'attr'  => 'exp',
   *       'value' => 'e-abc'
   *     ],
   *     [
   *       'attr'  => 'splash',
   *       'value' => 'img-only:6/5'
   *     ]
   * ]
   * Into this:
   * '<script src="//lib.cinema6.com/c6embed/v1/c6embed.min.js" exp="e-abc"
   *    splash="img-only:6/5"></script>'
   */
  $create_script_tag = _cinema6_minireel_pipeline([
    function($params) {
      return array_map(function($param) {
        if ($param['attr'] === 'version') {
          return [
            'attr' => 'src',
            'value' => '"//lib.cinema6.com/c6embed/v' .
              $param['value'] . '/c6embed.min.js"'
          ];
        }

        if ($param['attr'] !== $param['value']) {
          return [
            'attr' => 'data-' . $param['attr'],
            'value' => '"' . $param['value'] . '"'
          ];
        }
        else {
          return [
            'attr' => 'data-' . $param['attr']
          ];
        }
      }, $params);
    },
    function($params) {
      $params_to_string = _cinema6_minireel_pipeline([
        function($params) {
          return array_map(function($param) {
            return implode('=', $param);
          }, $params);
        },
        function($key_value_pairs) {
          return implode(' ', $key_value_pairs);
        }
      ]);

      return '<script ' . $params_to_string($params) . '></script>';
    }
  ]);

  $shortcode_to_script = _cinema6_minireel_pipeline([
    $arrayify_shortcode,
    $create_script_tag
  ]);

  /*
   * Finds all MiniReel shortcodes and replaces them with script tags.
   */
  $get_modified_content = _cinema6_minireel_pipeline([
    function($content, &$context) {
      $matches = [];

      $context = $content;

      preg_match_all('/\[minireel .+?\]/', $content, $matches);

      return $matches[0];
    },
    function($embed_codes) use ($shortcode_to_script) {
      return array_map(function($code) use ($shortcode_to_script) {
        return [
          'orig' => $code,
          'script' => $shortcode_to_script($code)
        ];
      }, $embed_codes);
    },
    function($nodes, $content) {
      return array_reduce($nodes, function($content, $node) {
        return str_replace($node['orig'], $node['script'], $content);
      }, $content);
    }
  ]);

  return $get_modified_content($html);
}
