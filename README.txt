Module: Cinema6 MiniReel

INTRODUCTION
------------
Finds instances of MiniReel shortcodes in the HTML e.g.:
[minireel version="1" exp="e-abc" splash="img-only:6/5"]

and converts them into MiniReel embed script tags e.g.:
<script src="//lib.cinema6.com/c6embed/v1/c6embed.min.js" data-exp="e-abc"
data-splash="img-only:6/5"></script>

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/minznerjosh/2328625
 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/node/add/project-issue/2328625


REQUIREMENTS
------------
This module requires the following to work:
  * Drupal 7
  * PHP >= 5.3.0


INSTALLATION
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------
The module has no menu or modifiable settings. There is no
configuration. When enabled, the module will convert MiniReel shortcodes in
posts to Cinema6 embed script tags.

How to use
==========
Create or edit an article and paste the minireel shortcode.
Set the Text Format to Filtered HTML or Full HTML.
Save and view the article.
